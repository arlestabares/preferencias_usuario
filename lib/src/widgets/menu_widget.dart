import 'package:flutter/material.dart';
import 'package:preferencias_usuario/src/pages/home_page.dart';
import 'package:preferencias_usuario/src/pages/settings_page.dart';

class MenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ListTile(
                  title: Text('Ingeniero Desarrollador'),
                  subtitle: Text('Arles Tabares'),
                )
              ],
            ),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/img/menu-img.jpg'),
                    fit: BoxFit.cover)),
          ),
          ListTile(
            leading: Icon(Icons.pages, color: Colors.blue),
            title: Text('Home'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, HomePage.routeName),
          ),
          ListTile(
            leading: Icon(Icons.party_mode, color: Colors.blue),
            title: Text('Pary Mode'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.people, color: Colors.blue),
            title: Text('People'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.settings, color: Colors.blue),
            title: Text('Settings'),
            onTap: () {
              //Opciones diferentes de navegar hacia la pagina principal
              // Navigator.pop(context);
              // onTap: () => Navigator.pushNamed(context, SettingsPage.routeName)

              Navigator.pushReplacementNamed(context, SettingsPage.routeName);
            },
          ),
          ListTile(
              leading: Icon(Icons.access_time, color: Colors.blue),
              title: Text('Tiempo'),
              onTap: () {}
              // Navigator.pushReplacementNamed(context, HomePage.routeName),
              ),
          ListTile(
            leading: Icon(Icons.add_location, color: Colors.blue),
            title: Text('Ubicacion'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.account_box, color: Colors.blue),
            title: Text('Cuenta'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.flight_land, color: Colors.blue),
            title: Text('Vuelos'),
            onTap: () {
              //Opciones diferentes de navegar hacia la pagina principal
              // Navigator.pop(context);
              // onTap: () => Navigator.pushNamed(context, SettingsPage.routeName)

              Navigator.pushReplacementNamed(context, SettingsPage.routeName);
            },
          ),
          ListTile(
              leading: Icon(Icons.laptop_mac, color: Colors.blue),
              title: Text('Laptop'),
              onTap: () {}
              // Navigator.pushReplacementNamed(context, HomePage.routeName),
              ),
          ListTile(
            leading: Icon(Icons.view_agenda, color: Colors.blue),
            title: Text('Mostrar Agenda'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.sim_card_alert, color: Colors.blue),
            title: Text('Alerta Sim Card'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.work, color: Colors.blue),
            title: Text('Trabajo'),
            onTap: () {
              //Opciones diferentes de navegar hacia la pagina principal
              // Navigator.pop(context);
              // onTap: () => Navigator.pushNamed(context, SettingsPage.routeName)

              // Navigator.pushReplacementNamed(context, SettingsPage.routeName);
            },
          ),
        ],
      ),
    );
  }
}
